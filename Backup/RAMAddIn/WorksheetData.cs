﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RAMAddIn
{
    [Serializable]
    public class WorksheetData
    {
        public string SheetName { get; set; }
        public object Username { get; set; }
        public object Password { get; set; }
        public object SiteName { get; set; }
        public object CompanyName { get; set; }
        public object Country { get; set; }

        public object UnitName { get; set; }
        public object TAAvgInterval { get; set; }
        public object TACausePcnt_FP { get; set; }
        public object TACausePcnt_IE { get; set; }
        public object TACausePcnt_RE { get; set; }
        public object TACausePcnt_Tot { get; set; }
        public object TACompressors { get; set; }
        public object TAContAvail { get; set; }
        public object TAControlValves { get; set; }
        public object TACptlProject { get; set; }
        public object TACraftmanMtCapWhrFP { get; set; }
        public object TACraftmanMtCapWhrIE { get; set; }
        public object TACraftmanMtCapWhrRE { get; set; }
        public object TACraftmanMtCapWhrTOT { get; set; }
        public object TACraftmanTotWhrFP { get; set; }
        public object TACraftmanTotWhrIE { get; set; }
        public object TACraftmanTotWhrRE { get; set; }
        public object TaskEmergency_RE { get; set; }
        public object TaskEmergency_Tot { get; set; }
        public object AnnTAMatlFP { get; set; }
        public object AnnTAMatlIE { get; set; }
        public object AnnTAMatlMtCapFP { get; set; }
        public object AnnTAMatlMtCapIE { get; set; }
        public object AnnTAMatlMtCapRE { get; set; }
        public object AnnTAMatlMtCapTOT { get; set; }
        public object AnnTAMatlRE { get; set; }
        public object AnnTAMatlTOT { get; set; }
        public object AnnTAMatlTotFP { get; set; }
        public object AnnTAMatlTotIE { get; set; }
        public object AnnTAMatlTotRE { get; set; }
        public object AnnTAMatlTotTOT { get; set; }
        public object EquipCntAgitatorsInVessels { get; set; }
        public object _EquipCntAnalyzerBlending { get; set; }
        public object EquipCntAnalyzerBlending { get; set; }
        public object _EquipCntAnalyzerEmissions { get; set; }
        public object EquipCntAnalyzerEmissions { get; set; }
        public object _EquipCntAnalyzerProcCtrl { get; set; }
        public object EquipCntAnalyzerProcCtrl { get; set; }
        public object EquipCntBlowersFans { get; set; }
        public object EquipCntCentrifugesMain { get; set; }
        public object _EquipCntCompressorsRecip { get; set; }
        public object EquipCntCompressorsRecip { get; set; }
        public object _EquipCntCompressorsRotating { get; set; }
        public object EquipCntCompressorsRotating { get; set; }
        public object _EquipCntControlValves { get; set; }
        public object EquipCntControlValves { get; set; }
        public object EquipCntCoolingTowers { get; set; }
        public object EquipCntCrushers { get; set; }
        public object _EquipCntDistTowers { get; set; }
        public object EquipCntDistTowers { get; set; }
        public object EquipCntDistVoltage { get; set; }
        public object _EquipCntFurnaceBoilers { get; set; }
        public object EquipCntFurnaceBoilers { get; set; }
        public object _EquipCntHeatExch { get; set; }
        public object EquipCntHeatExch { get; set; }
        public object EquipCntHeatExchFinFan { get; set; }
        public object EquipCntHeatExchOth { get; set; }
        public object EquipCntISBLSubStationTransformer { get; set; }
        public object _EquipCntMotorsMain { get; set; }
        public object EquipCntMotorsMain { get; set; }
        public object _EquipCntPumpsCentrifugal { get; set; }
        public object EquipCntPumpsCentrifugal { get; set; }
        public object _EquipCntPumpsPosDisp { get; set; }
        public object EquipCntPumpsPosDisp { get; set; }
        public object EquipCntRefrigUnits { get; set; }
        public object EquipCntRotaryDryers { get; set; }
        public object _EquipCntSafetyInstrSys { get; set; }
        public object EquipCntSafetyValves { get; set; }
        public object EquipCntSilosISBL { get; set; }
        public object EquipCntStorageTanksISBL { get; set; }
        public object _EquipCntTurbines { get; set; }
        public object EquipCntTurbines { get; set; }
        public object _EquipCntVarSpeedDrives { get; set; }
        public object EquipCntVarSpeedDrives { get; set; }
        public object _EquipCntVessels { get; set; }
        public object EquipCntVessels { get; set; }
        public object EventsAnalyzerBlending { get; set; }
        public object EventsAnalyzerEmissions { get; set; }
        public object EventsAnalyzerProcCtrl { get; set; }
        public object EventsCompressorsRecip { get; set; }
        public object EventsCompressorsRotating { get; set; }
        public object EventsControlValves { get; set; }
        public object EventsDistTowers { get; set; }
        public object EventsFurnaceBoilers { get; set; }
        public object EventsHeatExch { get; set; }
        public object EventsMotorsMain { get; set; }
        public object EventsPumpsCentrifugal { get; set; }
        public object EventsPumpsPosDisp { get; set; }
        public object EventsSafetyInstrSys { get; set; }
        public object EventsTurbines { get; set; }
        public object EventsVarSpeedDrives { get; set; }
        public object EventsVessels { get; set; }
        public object MTBFAnalyzerBlending { get; set; }
        public object MTBFAnalyzerEmissions { get; set; }
        public object MTBFAnalyzerProcCtrl { get; set; }
        public object MTBFCompressorsRecip { get; set; }
        public object MTBFCompressorsRotating { get; set; }
        public object MTBFControlValves { get; set; }
        public object MTBFDistTowers { get; set; }
        public object MTBFFurnaceBoilers { get; set; }
        public object MTBFHeatExch { get; set; }
        public object MTBFMotorsMain { get; set; }
        public object MTBFPumpsCentrifugal { get; set; }
        public object MTBFPumpsPosDisp { get; set; }
        public object MTBFSafetyInstrSys { get; set; }
        public object MTBFTurbines { get; set; }
        public object MTBFVarSpeedDrives { get; set; }
        public object MTBFVessels { get; set; }
        public object ProgramCBM_FP { get; set; }
        public object ProgramCBM_IE { get; set; }
        public object ProgramCBM_RE { get; set; }
        public object ProgramRCM_FP { get; set; }
        public object ProgramRCM_IE { get; set; }
        public object ProgramRCM_RE { get; set; }
        public object ProgramTimeBased_FP { get; set; }
        public object ProgramTimeBased_IE { get; set; }
        public object ProgramTimeBased_RE { get; set; }
        public object RelProcessImproveProg_FP { get; set; }
        public object RelProcessImproveProg_IE { get; set; }
        public object RelProcessImproveProg_RE { get; set; }
        public object RiskMgmtEquipRBI_FP { get; set; }
        public object RiskMgmtEquipRBI_IE { get; set; }
        public object RiskMgmtEquipRBI_RE { get; set; }
        public object RoutMatlFP { get; set; }
        public object RoutMatlIE { get; set; }
        public object RoutMatlMtCapFP { get; set; }
        public object RoutMatlMtCapIE { get; set; }
        public object RoutMatlMtCapRE { get; set; }
        public object RoutMatlMtCapTOT { get; set; }
        public object RoutMatlRE { get; set; }
        public object RoutMatlTOT { get; set; }
        public object RoutMatlTotFP { get; set; }
        public object RoutMatlTotIE { get; set; }
        public object RoutMatlTotRE { get; set; }
        public object RoutMatlTotTOT { get; set; }
        public object SeverityConstMaterials { get; set; }
        public object SeverityCorrosivity { get; set; }
        public object SeverityErosivity { get; set; }
        public object SeverityExplosivity { get; set; }
        public object SeverityFlammability { get; set; }
        public object SeverityFreezePt { get; set; }
        public object SeverityPressure { get; set; }
        public object SeverityProcessComplexity { get; set; }
        public object SeverityTemperature { get; set; }
        public object SeverityToxicity { get; set; }
        public object SeverityViscosity { get; set; }
        public object ShortOH2YrCnt { get; set; }
        public object _ShortOHAnnHrsDown { get; set; }
        public object ShortOHAnnHrsDown { get; set; }
        public object ShortOHAvgHrsDown { get; set; }
        public object ShortOHCausePcnt_FP { get; set; }
        public object ShortOHCausePcnt_IE { get; set; }
        public object ShortOHCausePcnt_RE { get; set; }
        public object ShortOHCausePcnt_Tot { get; set; }
        public object StoppagePcnt_FP { get; set; }
        public object StoppagePcnt_IE { get; set; }
        public object StoppagePcnt_RE { get; set; }
        public object StoppagePcnt_Tot { get; set; }
        public object _TAAvgAnnHrsDown { get; set; }
        public object TAAvgAnnHrsDown { get; set; }
        public object TAAvgHrsDown { get; set; }
        public object TAAvgHrsExecution { get; set; }
        public object TAAvgHrsShutdown { get; set; }
        public object TAAvgHrsStartup { get; set; }
        public object TACraftmanTotWhrTOT { get; set; }
        public object TACraftmanWhrFP { get; set; }
        public object TACraftmanWhrIE { get; set; }
        public object TACraftmanWhrRE { get; set; }
        public object TACraftmanWhrTOT { get; set; }
        public object TACritPath { get; set; }
        public object TACritPathOth { get; set; }
        public object TADeterioration { get; set; }
        public object TADiscretionaryInspect { get; set; }
        public object TADistTowersOpened { get; set; }
        public object TAFiredFurnaces { get; set; }
        public object TAFixedSchedule { get; set; }
        public object TAHeatExchOpened { get; set; }
        public object TALowDemand { get; set; }
        public object TAPumps { get; set; }
        public object TARegulatoryInspect { get; set; }
        public object TaskConditionMonitor_FP { get; set; }
        public object TaskConditionMonitor_IE { get; set; }
        public object TaskConditionMonitor_RE { get; set; }
        public object TaskConditionMonitor_Tot { get; set; }
        public object TaskPreventive_FP { get; set; }
        public object TaskPreventive_IE { get; set; }
        public object TaskPreventive_RE { get; set; }
        public object TaskPreventive_Tot { get; set; }
        public object TaskRoutCorrective_FP { get; set; }
        public object TaskRoutCorrective_IE { get; set; }
        public object TaskRoutCorrective_RE { get; set; }
        public object TaskRoutCorrective_Tot { get; set; }
        public object TaskTotal_FP { get; set; }
        public object TaskTotal_IE { get; set; }
        public object TaskTotal_RE { get; set; }
        public object TaskTotal_Tot { get; set; }
        public object TAUnplannedFailure { get; set; }
        public object TAValvesCalibrated { get; set; }
        public object TAValvesOpened { get; set; }
        public object TAWeather { get; set; }
        public object UnitInfoContinuosOrBatch { get; set; }
        public object _UnitInfoExchangeRate { get; set; }
        public object UnitInfoMaxDailyProdRate { get; set; }
        public object TaskEmergency_FP { get; set; }
        public object TaskEmergency_IE { get; set; }
        public object UnitInfoPhasePredominentFeedstock { get; set; }
        public object UnitInfoPhasePredominentProduct { get; set; }
        public object UnitInfoPlantAge { get; set; }
        public object UnitInfoProdRateUOM { get; set; }
        public object UnitInfoPRV { get; set; }
        public object UnitInfoPRVUSD { get; set; }
        public object RoutCraftmanMtCapWhrFP { get; set; }
        public object RoutCraftmanMtCapWhrIE { get; set; }
        public object RoutCraftmanMtCapWhrRE { get; set; }
        public object RoutCraftmanMtCapWhrTOT { get; set; }
        public object RoutCraftmanTotWhrFP { get; set; }
        public object RoutCraftmanTotWhrIE { get; set; }
        public object RoutCraftmanTotWhrRE { get; set; }
        public object RoutCraftmanTotWhrTOT { get; set; }
        public object RoutCraftmanWhrFP { get; set; }
        public object RoutCraftmanWhrIE { get; set; }
        public object RoutCraftmanWhrRE { get; set; }
        public object RoutCraftmanWhrTOT { get; set; }
        public object LossHrsForAnnTA { get; set; }
        public object LossHrsForProcessReliability { get; set; }
        public object LossHrsForShortOH { get; set; }
        public object LossHrsForTotalProdLoss { get; set; }
        public object LossHrsForUnschedMaint { get; set; }

        public object UnitInfoProcessType { get; set; }
        public object UnitInfoPrimaryProductName { get; set; }

        public object EquipCntTurbinesSpares { get; set; }
        public object EquipCntCompressorsRecipSpares { get; set; }
        public object EquipCntCompressorsRotatingSpares { get; set; }
        public object EquipCntPumpsCentrifugalSpares { get; set; }
        public object EquipCntHeatExchSpares { get; set; }
        public object LostProductionOutages { get; set; }
        public object LostProductionRateReductions { get; set; }
        public object NonMaintOutagesMaintEquivHours { get; set; }
        public object RAMCausePcnt_RE { get; set; }
        public object RAMCausePcnt_FP { get; set; }
        public object RAMCausePcnt_IE { get; set; }
        public object RAMCausePcnt_Tot { get; set; }
        public object EquipCriticalityRanking_RE { get; set; }
        public object EquipCriticalityRanking_FP { get; set; }
        public object EquipCriticalityRanking_IE { get; set; }
        public object ProgramCUI_RE { get; set; }
        public object ProgramCUI_FP { get; set; }
        public object ProgramCUI_IE { get; set; }
        public object ProgramIRThermography_RE { get; set; }
        public object ProgramIRThermography_FP { get; set; }
        public object ProgramIRThermography_IE { get; set; }
        public object ProgramVibrationAnalysis_RE { get; set; }
        public object ProgramAcousticEmission_RE { get; set; }
        public object ProgramAcousticEmission_FP { get; set; }
        public object ProgramAcousticEmission_IE { get; set; }
        public object ProgramOilAnalysis_RE { get; set; }
        public object ProgramOilAnalysis_IE { get; set; }
    }
}
